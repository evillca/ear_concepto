package com.demo.ear.annotation;


import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogueableAspect {

	org.slf4j.Logger log  = LoggerFactory.getLogger(getClass());

	@Pointcut("within(@com.demo.ear.annotation.Logueable *)")
	public void beanAnnotatedWithLogueable() {}

	@Pointcut("execution(public * *(..))")
	public void publicMethod() {}
	
	@Pointcut("publicMethod() && beanAnnotatedWithLogueable()")
	public void publicMethodInsideAClassMarkedWithAtLogueable() {}

	@Pointcut("publicMethod() && @annotation(com.demo.ear.annotation.Logueable)")
	public void methodLoguable() {}
	
	@Around("publicMethodInsideAClassMarkedWithAtLogueable() || methodLoguable()")
	public Object loggin(ProceedingJoinPoint p) throws Throwable {
		if (p.getStaticPart().getSignature() instanceof MethodSignature) {
			
			MethodSignature methodSignature = (MethodSignature) p.getStaticPart().getSignature();
			
			Logueable logueable = methodSignature.getMethod().getAnnotation(Logueable.class);
			if (logueable == null) {
				logueable = (Logueable) methodSignature.getDeclaringType().getDeclaredAnnotation(Logueable.class);
			}
			
			Level level = Level.valueOf(logueable.levelLog().toString());
			Logger logManager = LogManager.getLogger(this.log);

			logManager.log(level, "Init execution: "+ p.getSignature().getName() + "()");
			for (int i = 0; i < methodSignature.getParameterNames().length; i++) {
				logManager.log(level, "Param: " + methodSignature.getParameterNames()[i] + ", value: " + p.getArgs()[i]);
			}
			
			long startTime = System.nanoTime();
			Object result = p.proceed();
			long endTime = System.nanoTime();
			long duration = (endTime - startTime);
			long seconds = (duration / 1000) % 60;

			if (!methodSignature.getMethod().getReturnType().getSimpleName().equals("void")) {
				logManager.log(level, "return : " + (result != null ? result.toString() : null));
			}

			logManager.log(level, "End execution:" + p.getSignature().getName() + "(), time: "
					+ String.format("(0.%d seconds)", seconds));

			return result;
		}
		return p.proceed();
	}
}
