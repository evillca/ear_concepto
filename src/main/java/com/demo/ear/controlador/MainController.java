package com.demo.ear.controlador;

import java.util.concurrent.Future;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncAnnotationAdvisor;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.demo.ear.annotation.Logger;
import com.demo.ear.annotation.Logueable;

@Controller
@Logueable
public class MainController {

	@GetMapping("/hello")
	@ResponseBody
	@Logueable(levelLog = Logger.ERROR)
	public String helloWord() {
		
		return "Hello word!!!";
	}
	
	@GetMapping("/hola")
	@ResponseBody
	public String holaMundo() {
		 return "hola mundo!!!";
	}
	
	@GetMapping("/async")
	@ResponseBody
	@Async
	public Future<String> asyncMrthodWhithReturnType() {
		System.out.println(Thread.currentThread().getName());
		try {
			Thread.sleep(5000);
			return new AsyncResult<String>("Hola mundo");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return null;
	}
}
